% !TEX root = ../thesis.tex
\chapter{Conclusion}
\label{chapter:conclusion}

In this MS thesis, we proposed an algorithmic framework for anytime geometric motion planning on large,
dense roamdaps. We justified the benefits of using a roadmap that is very large and dense, along with some 
heuristic search techniques and lazy evaluations. We cast the problem of anytime planning on a roadmap to searching
for the shortest feasible path over a sequence of subgraphs of the roadmap. We proposed an algorithm for anytime planning
on a roadmap (of reasonable size) called Pareto-Optimal Motion Planner (POMP) that maintains a belief over configuration 
space and searches for paths Pareto-optimal in path length and collision measure. 

We have discussed issues with the individual ideas of densification and configuration space belief search
in \sref{sec:densification-discussion} and \sref{sec:cspacebelief-discussion} respectively. Here we take a brief
look at our overall approach.

The analysis and remarks we had for the densification strategies are agnostic to the underlying search algorithm
used, as long as it returns the shortest path on each subgraph provided to it. However, we particulary advocate for 
using POMP as the search algorithm.

POMP is well-suited for being run several times on the same configuration space and obstacle setting as the C-space
belief model gets more and more informed as more edges are evaluated. In practice, using the Geometric Near-neighbour Access Tree or GNAT~\citep{brin1995near} datastructure, the model updates become reasonably efficient and the increased information from them is typically worth the computational cost of updation. The fail-fast nature of POMP is useful for when the next batch does not have an improved feasible solution. Using densification with POMP leads to an algorithm with multi-level anytime behaviour which is even better for the effort-to-quality tradeoff. POMP itself has no sub-optimality guarantees for intermediate solutions but as it obtains the shortest feasible path, the overall guarantees via densification do stand.


\section{Future Work}
\label{sec:conclusion-fw}

An immediate direction of future work would be to implement anytime roadmap planners which use densification
strategies and POMP as the underlying search algorithm, and benchmark it against the same densification strategies
with a different search algorithm (the one used in \cref{chapter:densification}). The behaviour of POMP as the batches
get larger and larger, and the belief model gets more and more data points, would also be interesting to observe
and analyse.

In the roadmap densification regime, at the end of each batch, the samples to be added
for the next batch are already decided beforehand based on the generating sequence. However, 
as the belief model is continuously being updated, it induces a prior
over the samples yet to be added. Whether this prior can be useful while adding
the next batch is another interesting question to explore.


\section{Final Remarks}

Despite the recent advances in deep reinforcement learning for control and the advent of sensorimotor approaches~\citep{levine2016end,pinto2016supersizing}, a large number of industrial applications for manipulation and mobile robotics rely on efficient motion planning algorithms. While theoretical properties like \emph{asymptotic optimality} are interesting and informative, we would like to have good quality solutions over a range of problems and be able to analyse the finite-time behaviour of our algorithms. With this thesis, we have made 
some amount of progress towards that end.