% !TEX root = ../thesis.tex
\chapter{Background}
\label{chapter:background}

We briefly outline the various concepts and relevant prior works that this thesis is based on.

\section{Motion Planning}
\label{sec:background-planning}

As stated in \cref{chapter:intro}, motion planning is a fundamental problem in robotics. There are comprehensive texts on the subject~\citep{L06,latombe2012robot} that discuss the various developments in this field, from the origins in the classical piano mover's problem~\citep{schwartz1983piano}. We are specifically interested in \emph{geometric motion planning} which focuses on finding a valid path between start and goal configurations that is not in collision anywhere along the path. 

\subsection{Sampling-based motion planning}
\label{sec:background-planning-sampling}

Sampling-based planning approaches build a graph, or a roadmap, in the configuration space, where vertices are configurations and edges are local paths connecting configurations. 
A path is then found by traversing this roadmap while checking if the vertices and edges are collision free.
Initial algorithms such as  PRM~\citep{kavraki1996probabilistic} and RRT~\citep{LK99} were concerned with finding \emph{a feasible} solution. 
However, in recent years, there has been growing interest in finding high-quality solutions.
Karaman and Frazzoli~\citep{KF11} introduced variants of the PRM and RRT algorithms, called PRM* and RRT*, respectively and proved that, asymptotically,  the solution obtained by these algorithms converges to the optimal solution. 
However, the running times of these algorithms are often significantly higher than their non-optimal counterparts.
Thus, subsequent algorithms have been suggested to increase the rate of convergence to high-quality solutions.
They use different approaches such as 
lazy computation~\citep{bohlin2000path,janson2015fast,salzman2015asymptotically,dellin2016unifying},
informed sampling~\citep{GSB14},
pruning vertices~\citep{gammell2014batch},
relaxing optimality~\citep{SH16}, exploiting local information~\citep{choudhury2016regionally} %LBT-RRT
and lifelong planning together with heuristics~\citep{koenig2004lifelong}.

\subsection{Efficient path-planning algorithms}
\label{sec:background-planning-efficient}

We are interested in path-planning algorithms that attempt to reduce the amount of 
computationally expensive edge expansions performed in a search.
This is typically done using heuristics such 
as for A*~\citep{hart1968formal}, for
Iterative Deepening A*~\citep{K85} and for
Lazy Weighted A*~\citep{CPL14}.
Some of these algorithms, such as Lifelong Planning A*~\citep{koenig2004lifelong} allow recomputing the shortest path in an efficient manner when the graph undergoes changes.
\emph{Anytime} variants of A* such as
Anytime Repairing A*~\citep{likhachev2004ara}
and
Anytime Nonparametric A*~\citep{BSHG11}
efficiently run a succession of A* searches, each with an inflated heuristic. 
This potentially obtains a fast approximation and refines its quality as time permits.
However, there is no formal guarantee that these approaches will decrease search time and they may still search all edges of a given graph~\citep{WR12}.
For a unifying formalism of such algorithms relevant to explicit roadmaps, in settings where edge evaluations are expensive, and for additional references, see~\citep{dellin2016unifying}.

\subsection{Finite-time properties of sampling-based algorithms}
\label{sec:background-planning-finite}

Extensive analysis has been done on \emph{asymptotic} properties of sampling-based algorithms,
i.e. properties such as connectivity and optimality when the number of samples tends to infinity~\citep{KKL98, KF11}.

We are interested in bounding the quality of a solution obtained using a \emph{fixed} roadmap for a finite number of samples.
When the samples are generated from a \emph{deterministic} sequence, \citep[Thm2]{JIP15} give a closed-form solution bounding the quality of the solution of a PRM whose roadmap is an $r$-disk graph. The bound is a function of $r$, the number of vertices $n$ and the \emph{dispersion} of the set of points used. (See  for an exact definition of dispersion and for the exact bound given by \citep{JIP15}).

Similar bounds have been provided by~\citep{DMB15} when randomly sampled i.i.d points are used.
Specifically, they consider a PRM whose roadmap is an $r$-disk graph for a \emph{specific} radius $r = c \cdot \left(\log n / n\right)^{1/d}$ where $n$ is the number of points, $d$ is the dimension and $c$ is some constant.
They then give a bound on the probability that the quality of the solution will be larger than a given threshold.

\section{Dispersion}
\label{sec:background-dispersion}

The \emph{dispersion} $D_n(\calS)$ of a sequence $\calS$ is defined as 
\[
D_n(\calS) = \sup_{x \in \calX} \min_{s \in \calS} \rho(x, s) 
\]
Intuitively, it can be thought of as the radius of the largest empty ball (by some metric)
that can be drawn around any point in the space $\calX$ without intersecting any point
of $\calS$. A lower dispersion implies a better \emph{coverage} of the space by the points in $\calS$.
When $\calX$ is the $d$-dimensional 
Euclidean space and $\rho$ is the Euclidean distance, deterministic sequences  with dispersion of order $O(n^{-1/d})$ exist.
A simple example is a set of points lying on grid or a lattice.

Other low-dispersion deterministic sequences exist which also  have low discrepancy, i.e. they appear to be random for many purposes.
One such example is the \emph{Halton sequence}~\citep{H60}. We will use them extensively for our analysis because
they have been studied in the context of deterministic motion planning~\citep{JIP15,BLOY01}. 
Halton sequences are constructed by taking $d$ prime numbers, called generators, one for each dimension.
Each generator $g$ induces a sequence, called a Van der Corput sequence.
The $k$'th element of the Halton sequence is then constructed by taking the $k$'th element of each of the $d$ Van der Corput sequences.
For Halton sequences, tight bounds on dispersion exist. Specifically, $D_n(\calS) \leq p_d \cdot n^{-1/d}$ where $p_d \approx d \text{log } d$ is the $d^{th}$ prime number.
Subsequently in this paper, we will use $D_n$ (and not~$D_n(\calS)$) to denote the dispersion of the first $n$ points of~$\calS$.


Previous work bounds the length of the shortest path computed over an $r$-disk roadmap constructed using a low-dispersion deterministic sequence~\citep[Thm2]{JIP15}.
Specifically, given start and target vertices, consider all paths $\Gamma$ connecting them which 
have $\delta$-clearance for some $\delta$. Set $\delta_{\text{max}}$ to be the maximal clearance over all such $\delta$.
If $\delta_{\text{max}} > 0$, then
for all $0<\delta \leq \delta_{\text{max}}$ 
set $c^*(\delta)$ to be the cost of the shortest path in $\Gamma$ with $\delta$-clearance.
Let $c(\ell,r)$ be the length of the path returned by a shortest-path algorithm on $\calG(\ell,r)$ with $\calS(\ell)$ having dispersion $D_\ell$.
For $2D_\ell < r < \delta$, we have that
\begin{equation}
\label{eq:dispersion_suboptimality}
c(\ell,r)
\leq 
\left( 
	1  + \frac{2D_\ell}{r - 2D_\ell}
\right) \cdot c^*(\delta).
\end{equation}

Notably, for $n$ random i.i.d. points, 
the lower bound on the dispersion is $O\left( (\log n / n )^{1/d}\right)$~\citep{N92} which is strictly larger than for deterministic samples.

For domains other than the unit hypercube, the insights from the analysis will generally hold. However, the dispersion bounds may become far more complicated depending on the domain, and the distance metric would need to be scaled accordingly. This may result in the quantitative bounds being difficult to deduce analytically.

\section{Configuration Space Belief}
\label{sec:background-belief}

The probability of collision of a path is derived from an approximate model of the configuration space of the robot. Since we explicitly seek to minimize collision checks, we build up an incremental model using data from previous collision tests, instead of sampling several, potentially irrelevant configurations apriori. This idea has been studied \citep{burns2003information,burns2005sampling} in similar contexts. Furthermore, the evolving probabilistic model can be used to guide future searches towards likely free regions. Previous work has analyzed and utilized this exploration-exploitation paradigm for faster motion planning \citep{rickert2008balancing,knepper2012real,pan2013faster,arslan2015dynamic}.

\section{Multi-objective Path Search}
\label{sec:background-multiobj}

We reason about both the path length and the probability of collision for an individual candidate path. This analysis is built upon a considerable body of work dealing with bi-criteria path problems. Early work has conducted a systematic study of these problems \citep{hansen1980bicriterion}, and devised methods to obtain non-dominated or Pareto optimal paths \citep{climaco1982bicriterion}. It has also provided insights directly relevant to shortest path problems for robots\citep{mitchell2003continuous}.