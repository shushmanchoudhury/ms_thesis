% !TEX root = ../thesis.tex
\chapter{Introduction}
\label{chapter:intro}

Motion planning is one of the fundamental aspects of robotics. Given some desired start and goal configuration of the robot, the motion planning problem seeks to obtain a sequence of discrete movements that satisfy the physical constraints of the robot and the environment and (maybe) additionally optimizes some objective function (see~\figref{fig:intro-mp} for some examples). There are multiple reference texts devoted to motion planning~\citep{latombe2012robot,L06} and we will not re-invent the wheel for the purpose of this thesis.

\begin{marginfigure}
	\centering
	\vspace{30ex}
	\subfloat[\url{http://users.wpi.edu/~dberenson}]{
		\includegraphics[width=\textwidth]{figures/introduction/mp1}
		\label{fig:intro-mp1}
	}\\
	\subfloat[\url{http://coppeliarobotics.com}]{
		\includegraphics[width=\textwidth]{figures/introduction/mp2}
		\label{fig:intro-mp2}
	}\\
	\subfloat[\url{http://ompl.kavrakilab.org}]{
		\includegraphics[width=\textwidth]{figures/introduction/mp3}
		\label{fig:intro-mp3}
	}
	\caption[Examples of motion planning problems]
	{
		Three typical examples of motion planning problems - \protect\subref{fig:intro-mp1} a point robot 
		in a 2D world \protect\subref{fig:intro-mp2} the end-effector of a manipulator and
		\protect\subref{fig:intro-mp3} an autonomous vehicle in an outdoor environment.
	}
	\label{fig:intro-mp}
\end{marginfigure}


We focus on a specific sub-part of the motion planning problem, geometric motion planning on roadmaps. In \emph{geometric} motion planning, we only consider the state-space constraints of the robot. All configurations in the plan must be within the state-space limits and must not put the robot in collision with the environment or with itself. The collision-free solution path to a geometric motion planning problem is typically not enough for execution in most robotic systems - it must subsequently be converted to a trajectory which also satisfies the dynamical constraints of the system.

The term \emph{roadmap} refers to one of the popular constructs to solve the geometric motion planning problem. Roadmap-based methods are an example of the \emph{sampling-based} approach to motion planning, which samples points in the robot's configuration space rather than explicitly constructing a representation of the obstacles\citep{lozano1983spatial}, which typically cannot be done analytically for arbitrary configuration spaces. The feasibility of configurations is verified using a collision detection module, which is treated as a black box by the motion planning algorithm. A roadmap is a topological graph where the vertices are sampled configurations of the robot, and the edges are connections between vertices that can be created by some local planner. Given start and goal configurations, the geometric motion planning problem is solved by connecting the start and goal to the roadmap and finding a feasible path on the roadmap between start and goal using some graph search algorithm~\citep{dijkstra1959note,hart1968formal}. 


The basic idea behind roadmaps was introduced with the term \emph{probabilistic roadmaps} (PRM)~\citep{kavraki1996probabilistic}. The basic idea behind the PRM approach is to construct the roadmap graph in configuration space, remove all vertices and edges that are in collision in an offline \emph{pre-processing} stage and then search for the shortest path between start and goal configurations in an online \emph{query} stage. It is particularly well suited to multi-query settings where the environment does not change but multiple start-goal pairs are generated, for which the shortest feasible path needs to be found.

\begin{marginfigure}
	\centering
	\includegraphics[width=\textwidth]{figures/introduction/prm}
	\label{fig:intro-prm}
	\caption[A probabilistic roadmap visualized]
	{
		Courtesy \url{https://sites.google.com/site/moslemk}.
		An example of a probabilistic roadmap used to solve a 2D motion planning problem.
	}
\end{marginfigure}

\vspace{\baselineskip}

\noindent
\textbf{N.B - } To avoid any confusion, please note that we consider the LazyPRM~\citep{bohlin2000path} setting rather than the classical PRM setting in that we do not have a phase where we remove all edges and nodes in collision with the environment before beginning the plan. That would not be practical in the real-time case where we want a solution as soon as possible after observing the environment. Once we obtain an environment on which we want to solve some planning problem, we evaluate nodes and edges just-in-time (or lazily) for that specific problem. 
\vspace{\baselineskip}

We ideally want a geometric motion planning algorithm to obtain \emph{good quality paths over a wide range of motion planning problems}. What does that mean in terms of roadmaps? To be able to find any feasible path over a diverse set of problems, we would want the roadmaps to be \emph{large}, with a very high number of vertices (samples) so as to cover the configuration space sufficiently (think of the number vertices $N$ being of the order $10^6$ or $10^7$). And to get the best quality paths possible with those vertices, we would want the roadmaps to be \emph{dense} or even \emph{complete}, i.e. with a potential edge between every pair of vertices.


The problem with large, dense roadmaps is that any shortest path algorithm that directly searches over the entire roadmap graph may be too computationally expensive to be practical in real-time. Therefore we consider \emph{anytime motion planning}, which finds an initial solution quickly and uses its length (or some other relevant objective function) as a bound for future solutions.

\begin{quote}\em\normalsize
    This thesis proposes an algorithmic framework for anytime geometric motion planning on large, dense roadmaps.
\end{quote}


\begin{marginfigure}
	\centering
	\vspace{-30ex}
	\includegraphics[width=\textwidth]{figures/introduction/robot_herb}
	\caption[HERB, our lab's robot]
	{
		A (rather outdated) snapshot of the Personal Robotics Laboratory's robot HERB (Home Exploring Robot Butler).
	}
	\label{fig:intro-herb}
\end{marginfigure}


Though our approach, algorithms and analyses are all completely general, in terms of applications we are particularly motivated by motion planning for manipulators, such as for HERB~\citep{srinivasa2010herb}, shown in \figref{fig:intro-herb}.

\section{Observations for Manipulation Planning}
\label{sec:intro-observations}

We make three important observations about manipulation planning that inform our general approach to the anytime motion planning problem, and the particular aspects and challenges to consider.

\observation{Manipulators have several degrees of freedom}
\label{obs:dofs}

Most typical manipulators have many degrees of freedom, one for each independently controlled joint. For instance, the Barrett WAM arm~\citep{smith2006harmonious} which is used by HERB as well as several other research platforms, has $7$ active DOFs. The same is true for each arm of the PR2, excluding the additional DOF at the wrist. This makes the planning problem fairly high-dimensional. The number of samples needed to cover a space grows exponentially with the dimensionality of the problem. Therefore roadmaps for manipulation planning need to be especially large.

\observation{Collision Checks can be very expensive}
\label{obs:checks}

For manipulators, collision checking typically involves processing a triangular mesh model of the manipulator as well as models of the obstacles in the simulator that is used. The more articulated the robot, the more complex the mesh model, and the more computationally expensive the collision check. In the classical PRM approach, a significant fraction of time is spent on collision checking~\citep{sanchez2002delaying}, irrespective of the problem setting. This issue is further exacerbated when the collision checks are particularly expensive. On roadmaps, this is manifested as \emph{expensive edge evaluations} because evaluating an edge involves checking several embedded configurations.

\begin{marginfigure}
	\centering
	\includegraphics[width=\textwidth]{figures/introduction/robot_mesh}
	\label{fig:intro-mesh}
	\caption[An articulated robot mesh]
	{
		A triangular mesh model for an articulated robot can often be extremely complex. This makes collision checks,
		which need to process the entire mesh model in the worst case, very computationally expensive.
	}
\end{marginfigure}


\observation{The tradeoff between execution time and planning time is crucial}
\label{obs:ptime}

Optimality and bounded sub-optimality are typically considered to be important properties for motion planning algorithms. However, manipulation planning on large dense roadmaps is perhaps the most appropriate example where the execution speedup obtained by the shortest (or approximately shortest) path may be negated by the extra planning time required to find it. For a manipulation planning problem, the configuration space of interest is a subset of the swath of space that the manipulator can carve out. The difference between the shortest and any non-shortest path is much smaller for a manipulation planning problem than for say, a mobile robot or aerial vehicle planning problem. Therefore the tradeoff between execution time (represented by the path length) and planning time (represented by the effort to find a path) becomes particularly important~\citep{dellin2016completing}.


\section{Key Ideas}
\label{sec:intro-ideas}

Our approach to anytime geometric motion planning on roadmaps is based on two key ideas. They are motivated to some extent by the specific challenges for manipulation planning but as such are completely general and can be applied to any geometric motion planning setting. We outline them here.

\idea{Anytime planning as a search over a sequence of subgraphs}

Our key insight for solving the anytime planning problem in large, dense roadmaps is to provide existing path-planning algorithms with a sequence of increasingly dense subgraphs of the roadmap graph, using some \emph{densification strategy}~\citep{choudhury2016densification}. At each iteration, we run a shortest-path algorithm on the current subgraph to obtain an increasingly tighter approximation of the true shortest path (\figref{fig:intro-flow}).

\begin{marginfigure}
	\centering
	\includegraphics[width=\textwidth]{figures/introduction/densif_flow}
	\label{fig:intro-flow}
	\caption[The overall idea of densification]
	{
		The idea behind densification is to search for the shortest path over a sequence of increasingly dense subgraphs of
		the entire roadmap graph.
	}
\end{marginfigure}

Existing approaches to anytime planning on graphs~\citep{likhachev2004ara,likhachev2005anytime,BSHG11} typically modify the objective function in some way, thereby sacrificing optimality for a quicker solution. Some of those approaches guarantee bounded sub-optimality instead. This is quite reasonable to do when the graph that is being searched over is of a moderate size. But a single search with a modified objective function has a worst case complexity of $O(|V|\text{ log } |V| + |E|) \equiv O(N^2)$ which may be unacceptable for real-time applications, when dealing with large, dense roadmaps. Also, there is no formal guarantee that these approaches will decrease search time and they may still search all edges of a given graph~\citep{WR12}.

Instead, we consider explicitly solving sub-problems of the entire problem, and gradually increasing the size of each sub-problem until we solve the original problem. On a roadmap graph, this naturally maps to searching over increasingly dense subgraphs of the entire roadmap graph. The nature of the subgraph may be able to bound the sub-optimality of the solutions obtained, and more importantly, the tradeoff between the worst-case search effort and bounded sub-optimality. This is analysed in more detail in \cref{chapter:densification}.

\idea{Searching over configuration space beliefs}

Performing a collision check provides exact information but is computationally expensive. We assume that the roadmaps we search over are embedded in a continuous ambient space, where nearby points tend to share the same collision state. Therefore, we can use a model of the world to estimate the probability of unevaluated configurations to be free or in collision.  We ensure that updating and querying the model is inexpensive. Searching for paths based on collision probability does not guarantee optimality, but may speed up the computation of some feasible path. Furthermore, we search for paths that are pareto-optimal in collision probability and path length, adjusting the tradeoff between them to eventually obtain the shortest path on the graph we are searching~\citep{choudhury2016pareto}. See \figref{fig:intro-pompfig}. We propose using this search technique, which we will elaborate upon in \cref{chapter:cspacebelief}, for searching each individual subgraph generated by the densification strategy.

We want to try and minimize the number of collision checks while searching for feasible paths in a roadmap. A number of previous works have tried to address this  - using the probability of collision as a heuristic to guide the search over paths to obtain a feasible path\citep{nielsen2000two}; lazily and optimistically searching for the shortest path in a roadmap \citep{bohlin2000path,dellin2016unifying}; probabilistically modelling obstacle locations to combine exploration and exploitation in a hybrid approach \citep{knepper2012real}; using collision probabilities learned from previous instances to modify the roadmap cost function, and filter out unlikely configurations \citep{pan2013faster}.

\begin{marginfigure}
	\centering
	\includegraphics[width=\textwidth]{figures/introduction/pomp_fig}
	\label{fig:intro-pompfig}
	\caption[The overall idea of searching over configuration space beliefs]
	{
		We reason about the tradeoff between path length and collision measure (related to probability)
		for candidate paths. Specifically, we efficiently search for paths that are pareto-optimal in
		the two quantities and select them for lazy evaluation.
	}
\end{marginfigure}

Past work lacks, however, a way to connect the two problems of finding a feasible path quickly and finding the shortest feasible path in the roadmap. Our key insight is that this can be achieved by balancing the probability of collision and the length of a path in the objective. We show that this behaviour is approximately equivalent to searching for paths of minimum expected cost, with a gradually decreasing penalty of being in collision.

\section{Contributions}
\label{sec:contributions}

This thesis proposes an algorithmic framework for anytime geometric motion planning in large, dense roadmaps that is particularly well suited for the difficult high-dimensional setting of manipulation. Based on our key ideas, the following are our contributions:

\begin{itemize}

	\item We motivate the use of a large, dense roadmap for motion planning. In particular we propose using the same roadmap constructed for a particular robot's configuration space over a wide range of problems that the robot may be required to solve. This lets us take advantage of the structure embedded in the roadmap, as well as various preprocessing benefits of using the same roadmap (\cref{chapter:approach}).

	\item We frame the problem of anytime planning on a roadmap as searching over a sequence of subgraphs of the entire roadmap graph. We discuss several densification strategies to generate this sequence of subgraphs. For the specific case where the samples are generated from a low-dispersion (\sref{sec:background-dispersion}) deterministic sequence, we analyse the tradeoff between effort and bounded sub-optimality (\cref{chapter:densification}, \citet{choudhury2016densification})

	\item We present an anytime planning algorithm (for a reasonably sized roadmap) that maintains a belief over configuration space collision probabilities and efficiently searches for paths that are pareto-optimal in collision probability and path length (\cref{chapter:cspacebelief}, \citet{choudhury2016pareto}).

\end{itemize}

\noindent We conclude in \cref{chapter:conclusion} by discussing an overall perspective of our approach and its limitations, as well as interesting questions for future research.