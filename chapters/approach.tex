% !TEX root = ../thesis.tex
\chapter{Problem Definition}
\label{chapter:approach}

Let~$\calX$ denote a $d$-dimensional C-space, $\Cfree$ the collision-free portion of $\calX$, 	$\Cobs = \calX \setminus \Cfree$ 
its complement
and
let $\zeta: \calX \times \calX \rightarrow \R$ be some distance metric.
For simplicity, we assume that $\calX = [0,1]^d$ and that $\zeta$ is the Euclidean norm.
Let  $\calS = \{ s_1, \ldots, s_n\}$ be some sequence of points 
where $s_\ell \in \calX$
for some integer $n \in \N$ 
and denote by $\calS(\ell)$ the first $\ell$ elements of $\calS$. 
We define the $r$-disk graph $\calG(\ell, r) = (V_{\ell},E_{\ell,r})$ 
where
$V_{\ell} = \calS(\ell)$, 
$E_{\ell,r} = \{(u,v) \ | \ u,v \in V_\ell \text{ and } \zeta(u,v) \leq r \}$
and each edge $(u,v)$ is of length $\zeta(u,v)$.
See~\citep{KF11, SSH16c} for various properties of such graphs in the context of motion planning.
Our definition assumes that $\calG$ is embedded in $\calX$. 
Set~$\calG = \calG(n, \sqrt{d})$, namely, the complete\footnote{Using a radius of $\sqrt{d}$ ensures that every two points will be connected due to the assumption that $\calX = [0,1]^d$ and that $\zeta$ is Euclidean.} graph defined over $\calS$.


We propose using the same roadmap (with online, lazy evaluations for each specific environment)
over a range of problem instances. As mentioned in \cref{chapter:intro}, we cannot
do the pre-processing in the sense of the classical PRM approach due to real-time constraints.
However, using a fixed roadmap structure allows us to do some environment-agnostic preprocessing
that other classes of approaches like tree-growing~\citep{kuffner2000rrt} or trajectory
optimization~\citep{ratliff2009chomp} cannot do.


We can pre-compute all the nearest neighbors for each of the vertices
in the roadmap, and filter out the configurations in self-collision, thereby
requiring us to only check for robot-environment collisions during planning.
Previous work has shown that both the computation of nearest neighbours~\citep{kleinbort2016collision}
and detecting self-collision~\citep{srinivasa2016system} are expensive
components of motion planning algorithms.


As we have motivated earlier in \cref{chapter:intro}, we would need to use \emph{large dense}
motion-planning roadmaps to get good quality solutions over a wide range of problems. An illustration
of why this is necessary is shown with some $\mathbb{R}^{2}$ problems in \figref{fig:approach-motiv}.
For ease of analysis we assume that the roadmap is complete, but our densification strategies and analysis can be extended to \emph{dense} roadmaps that are not complete.


To save computation, although we create the graph explicitly, we do not evaluate it apriori, so we do not know if any of its vertices or edges are in $C_{\text{free}}$ or $C_{\text{obs}}$. This setup is similar to that of the LazyPRM \citep{bohlin2000path}, which then searches over the graph optimistically until it finds the shortest feasible path.


\begin{figure*}[t]
	\subfloat[]{
		\includegraphics[width=0.3\textwidth]{figures/approach/motiv_prob1}
		\label{fig:approach-motiv1}
	}
	\subfloat[]{
		\includegraphics[width=0.3\textwidth]{figures/approach/motiv_prob2}
		\label{fig:approach-motiv2}
	}
	\subfloat[]{
		\includegraphics[width=0.3\textwidth]{figures/approach/motiv_prob3}
		\label{fig:approach-motiv3}
	}
	\caption[Motivation for using large dense roadmaps]
	{
		Consider the range of toy problems shown here : \protect\subref{fig:approach-motiv1} A few obstacles with large gaps between them \protect\subref{fig:approach-motiv2} A single narrow passage 
		\protect\subref{fig:approach-motiv3} Several obstacles with narrow gaps between them.
		A large dense roadmap is necessary to find 
		feasible paths of good quality over such a diverse set of problems.
	}
	\label{fig:approach-motiv}
\end{figure*}


A query $\calQ$ is a scenario with start and target configurations. 
Let the start and target configurations be $s_1$ and $s_2$, respectively. 
The obstacles induce a mapping $\calM: \calX \rightarrow \{\Cfree,\Cobs\}$ called a \emph{collision detector} which checks if a configuration or edge is collision-free or not.
Typically, edges are checked by densely sampling along the edge, and performing expensive collision checks for each sampled configuration, hence the term \emph{expensive edge evaluation}. 
A feasible path is denoted by $\gamma: [0,1] \rightarrow \Cfree$ where $\gamma[0] = s_1$ and $\gamma[1] = s_2$.
In terms of the graph, a path is \emph{feasible} if every included edge is in $\Cfree$.
Slightly abusing this notation, 
set $\gamma(\calG(\ell, r))$ to be the shortest collision-free path from $s_1$ to $s_2$ that can be computed in $\calG(\ell, r)$,
its clearance as $\delta(\calG(\ell, r))$ and denote by
$\gamma^* = \gamma(\calG)$ and
$\delta^* = \delta(\calG)$
the shortest path and its clearance that can be computed in $\calG$, respectively.
Note that a path has clearance $\delta$ if every point on the path is at a distance of at least
$\delta$ away from every obstacle.

Our problem calls for finding a sequence of increasingly shorter
feasible paths  $\gamma_0$, $\gamma_1 \ldots$ in $\calG$,  converging
to $\gamma^{*}$.
We assume that $n = |\calS|$ is sufficiently large, and the roadmap covers the space well enough so that for any reasonable set of obstacles, there are multiple feasible paths to be obtained between start and goal. Therefore, we do not consider a case where the entire roadmap is invalidated by obstacles. The large value of $n$ makes any path-finding algorithm that directly searches~$\calG$, thereby performing $O(n^2)$ calls to
the collision-detector, too time-consuming to be practical.

