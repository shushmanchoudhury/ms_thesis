% !TEX root = ../thesis.tex
\chapter{Densification}
\label{chapter:densification}

(\emph{This chapter is based on work presented in~\citep{choudhury2016densification}}.)

\noindent
As mentioned in \cref{chapter:approach}, a \emph{densification strategy} is a means to traverse the
space of $r$-disk subgraphs of the entire roadmap graph, selecting subgraphs along the way to be
searched, all the way to the fully connected roadmap. In this chapter, we discuss our general approach of searching over the space of all ($r$-disk) subgraphs of $\calG$.


We start by characterizing the boundaries and different regions of this space. 
Subsequently, we introduce two densification strategies---edge batching and vertex batching.
As we will see, these two are complementary in nature, which motivates our third strategy, which we call hybrid batching.

\begin{marginfigure}
    \centering
    \vspace{25ex}
    \includegraphics[width=\textwidth]{figures/densification/ve-batching2}
    \caption[The overall idea of densification]{Our meta algorithm leverages existing path-planning algorithms and provides them with a sequence of subgraphs.
    To do so we consider densification strategies for traversing the space of $r$-disk subgraphs
    of the roadmap $\calG$. ,
    The $x$-axis and the $y$-axis represent the number of vertices and the number of edges (induced by $r$) of the subgraph, respectively. A particular subgraph is defined by a point in this space.
    \emph{Edge batching} searches over all samples and adds edges according to an increasing radius of connectivity. 
    \emph{Vertex batching} searches over complete subgraphs induced by progressively larger subsets of vertices.
    \emph{Hybrid batching} uses the minimal connection radius $f(|V|)$ to ensure connectivity until it reaches~$|V| = n$ and then proceeds like edge batching.}
    \label{fig:ve_batching}
\end{marginfigure}

\section{The space of subgraphs}
\label{densification-space}

To perform an anytime search over $\calG$, 
given the collision detector $\calM$, 
we iteratively search a sequence of graphs
$\calG_0(n_0, r_0) \subseteq \calG_1(n_1, r_1)  \subseteq  \ldots \subseteq \calG_m(n_m, r_m) = \calG$.
If no feasible path exists in the subgraph, we move on to the next subgraph in the sequence, which is more likely to have a feasible path.

We use an incremental path-planning algorithm that allows us to efficiently recompute shortest paths.
Our problem setting of increasingly dense subgraphs is particularly amenable to such algorithms.
However, any alternative shortest-path algorithm may be used. 
We emphasize again that we focus on the meta-algorithm of choosing which subgraphs to search. Further details on the implementation of these approaches are provided in \sref{sec:densification-implementation}.

\figref{fig:ve_batching} depicts the set of possible graphs~$\calG(\ell, r)$ for all choices of  $0 < \ell \leq n$ and $0 < r \leq \sqrt{d}$.
Specifically, the graph depicts $|E_{\ell, r}|$ as a function of $|V_\ell|$.
We discuss it in detail to motivate our approach for solving the problem of anytime planning on large, dense roadmaps and the specific sequence of subgraphs we use.
First, consider the curves that define the boundary of all possible graphs:
The vertical line $|V| = n$ corresponds to subgraphs defined over the entire set of vertices, where batches of edges are added as $r$ increases.
The parabolic arc $|E| = |V|\cdot(|V|-1)/2$, corresponds to complete subgraphs defined over increasingly larger sets of vertices.

Recall that we wish to approximate the shortest path $\gamma^*$ which has some minimal clearance $\delta^*$.
Given a specific graph, to ensure that a path that approximates $\gamma^*$ is found, 
two conditions should be met:
(i)~The graph includes some minimal number $n_{\text{min}}$ of vertices. The exact value of~$n_{\text{min}}$ will be a function of the dispersion $D_{n_{\text{min}}}$ of the sequence $\calS$ and the clearance $\delta^*$.
(ii)~A minimal connection radius $r_0$ is used to ensure that the graph is connected.
Its value will depend on the sequence $\calS$ (and not on~$\delta^*$).

\begin{marginfigure}
    \includegraphics[width=\textwidth]{figures/densification/ve-regions2.pdf}
    \caption[Regions of interest for the space of subgraphs]{\emph{Vertex Starvation} happens in the region
    with too few vertices to ensure a solution, even for a fully connected subgraph. \emph{Edge Starvation}
    happens in the region where the radius $r$ is too low to guarantee connectivity.}
    \label{fig:ve_starvation}
\end{marginfigure}

In \figref{fig:ve_starvation}, requirement (i) induces a vertical line at $|V| = n_{\text{min}}$.
Any point to the left of this line corresponds to a graph with too few vertices to prove any guarantee that a solution will be found. We call this the \emph{vertex-starvation} region.
Requirement~(ii) induces a curve $f(|V|)$ such that any point below this curve corresponds to a graph which may be disconnected. 
We call this the \emph{edge-starvation} region.
The exact form of the curve depends on the sequence~$\calS$ that is used. 
Any point outside the starvation regions represents a graph~$\calG(\ell,r)$ such that the length of $\gamma(\calG(\ell,r))$ may be bounded.
For a discussion on specific bounds, see \sref{sec:densification-analysis-starvation}

\section{Densification Strategies}
\label{sec:densification-strategies}

Our goal is to search increasingly dense subgraphs of~$\calG$. 
This corresponds to a sequence of points on the space of subgraphs (\figref{fig:ve_starvation}) that ends at the upper right corner of the space. 
We discuss three general densification strategies, and defer the discussion on the choice of parameters used for each strategy to \sref{sec:densification-implementation}

\subsection{Edge Batching}
\label{sec:densification-strategies-eb}

All subgraphs include the complete set of vertices $\calS$ and the edges are incrementally added
via an increasing connection radius.
Specifically, $\forall i~n_i = n$ and $r_{i} = \eta_{e} r_{i-1}$ where
$\eta_{e} > 1$ and $r_{0}$ is some small initial radius. 
Here, we choose
$r_0 = O(f(n))$, where $f$ is the \emph{edge-starvation} boundary
curve defined previously. It defines the minimal radius to ensure connectivity
(in the asymptotic case) using $r$-disk graphs.
Specifically, 
$f(n) = O\left(n^{-1/d}\right)$ for low-dispersion deterministic sequences
and 
$f(n) = O\left( \left(\log n / n\right)^{-1/d}\right)$
for random i.i.d sequences~\citep{JIP15, KF11, SSH16b}.
Using \figref{fig:ve_starvation}, this induces a sequence of points along the vertical line at $|V| = n$ starting from $|E| = O(n^2 r_0^d)$ and ending at $|E| = O(n^2)$.


\begin{figure*}[t]
    \subfloat[40 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/easy_vertexbatch_40checks.png}}
        \label{fig:viz2d_vertex_easy1}
    }
    \subfloat[953 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/easy_vertexbatch_953checks.png}}
        \label{fig:viz2d_vertex_easy2}
    }
    \subfloat[6310 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/easy_vertexbatch_6310checks.png}}
        \label{fig:viz2d_vertex_easy3}
    }
    \subfloat[2573 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/hard_vertexbatch_2573checks.png}}
        \label{fig:viz2d_vertex_hard1}
    }
    \subfloat[61506 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/hard_vertexbatch_61506checks.png}}
        \label{fig:viz2d_vertex_hard2}
    }
    \subfloat[164504 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/hard_vertexbatch_164504checks.png}}
        \label{fig:viz2d_vertex_hard3}
    }

    \subfloat[206 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/easy_edgebatch_206checks.png}}
        \label{fig:viz2d_edge_easy1}
    }
    \subfloat[676 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/easy_edgebatch_676checks.png}}
        \label{fig:viz2d_edge_easy2}
    }
    \subfloat[15099 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/easy_edgebatch_15099checks.png}}
        \label{fig:viz2d_edge_easy3}
    }
    \subfloat[1390 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/hard_edgebatch_1390checks.png}}
        \label{fig:viz2d_edge_hard1}
    }
    \subfloat[4687 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/hard_edgebatch_4687checks.png}}
        \label{fig:viz2d_edge_hard2}
    }
    \subfloat[78546 checks]{
        \frame{\includegraphics[width=0.15\textwidth]{figures/densification/viz2d/hard_edgebatch_78546checks.png}}
        \label{fig:viz2d_edge_hard3}
    }
    \caption[An example of edge vs vertex batching on easy vs hard 2D problems]{
    Visualizations of vertex batching (upper row) and edge batching (lower row) on easy (left pane)
    and hard (right pane)
    $\R^{2}$ problems respectively. The same set of samples $\calS$ is used in each case. For easy problems, vertex batching finds the 
    first solution quickly with a sparse set of initial samples. Additional heuristics hereafter help it converge to the optimum with fewer edge evaluations than edge batching. The harder problem has $10\times$ more obstacles and lower average obstacle gaps. Therefore, both vertex and edge batching require
    more edge evaluations for finding feasible solutions and the shortest path. In particular, vertex batching requires multiple iterations to find its first solution,
    while edge batching still does so on its first search, albeit with more collision checks than for the easy problem. Note that the coverage of collision checks only appears similar at the end due to resolution
    limits for visualization.
    }
    \label{fig:viz2d}
\end{figure*}


\subsection{Vertex Batching}
\label{sec:densification-strategies-vb}

In this variant, all subgraphs are complete graphs defined over increasing subsets of the complete set of vertices $\calS$.
Specifically $\forall i~r_i = r_{\text{max}} = \sqrt{d}$,
$n_i = \eta_{v} n_{i-1}$ where $\eta_{v} > 1$ and the base term $n_0$ is some small number of vertices. Because we have no priors about the obstacle density or distribution, the chosen $n_0$ is a constant and does not vary due to $n$ or due to the volume of $\Cobs$.
Using \figref{fig:ve_starvation}, this induces a sequence of points along the parabolic arc $|E| = |V|\cdot (|V|-1)/2$
starting from $|V| = n_0$ and ending at $|V| = n$. The vertices are chosen in the same
order with which they are generated by~$\calS$. So, $\calG_0$ has the first $n_0$ samples of
$\calS$, and so on.


\vspace{\baselineskip}
\noindent
Intuitively, the relative performance of these densification
strategies depends on problem hardness. 
We use the clearance of the shortest path, $\delta^{*}$, to represent the hardness of the problem.
This, in turn, defines $n_{\min}$ which bounds the vertex-starvation region.
Specifically we say that a problem is easy (resp. hard) when
$\delta^* \approx \sqrt{d}$ 
(resp. $\delta^* \approx \Omega(D_n(\calS))$).
For easy problems, with larger gaps between obstacles,
where  $\delta^{*} \approx O(\sqrt{d})$, 
vertex batching can find a solution quickly with fewer samples and long edges, thereby restricting the work done
for future searches.
In contrast, assuming that $n > n_{\min}$, edge batching will find a solution on the first iteration but 
the time to do so may be far greater than for vertex batching because the number of samples
is so large.
For hard problems
where  $\delta^{*} \approx O(D_n)$,
vertex batching may require multiple iterations until the number of samples it uses is large enough and it is out of the vertex-starvation region.
Each of these searches would exhaust the fully connected subgraph before terminating. 
This cumulative effort is expected to exceed that required by edge batching for the same problem, which is expected to find a feasible
albeit sub-optimal path on the first search.
A visual depiction of this intuition is given in \figref{fig:ve_comparitive}.
Since we are focused on problems
with expensive edge evaluations, we treat work due to edge evaluations
as a reasonable approximation of the total work done by the search.
An empirical example of this is shown in \figref{fig:viz2d}.

\subsection{Hybrid Batching}
\label{sec:densification-strategies-hb}

Vertex and edge batching exhibit generally complementary properties for 
problems with varying difficulty.
Yet, when a query~$\calQ$ is given, the hardness of the problem is not known a-priori.
In this section we propose a hybrid approach that exhibits favourable properties, regardless of the hardness of the problem.

\begin{marginfigure}
    \vspace{20ex}
    \includegraphics[width=\textwidth]{figures/densification/iso-contours.pdf}
    \caption[The effect of hardness on densification strategies]{\ A visualization of the work required by our densification strategies as a function of the problem's hardness. Here work is measured as the number of edges evaluated.
    This is visualized using the gradient shading where light gray (resp. dark grey) depicts a small (resp. large) amount of work.
    Assuming $n > n_{\min}$, the amount of work required by edge batching remains the same regardless of problem difficulty.
    For vertex batching the amount of work required depends on the hardness of the problem.
    Here we visualize an easy and a hard problem using~$n_{\min}$ (easy) and $n_{\min}$ (hard), respectively.}
    \label{fig:ve_comparitive}
\end{marginfigure}

This hybrid batching strategy commences by searching over a graph $\calG(n_0,r_0)$
where $n_0$ is the same as for vertex batching and  $r_0 = O(f(n_0))$. 
As long as $n_i < n$, the next batch has 
$n_{i+1} = \eta_{v}n_{i}$ and $r_{i} = O(f(n_{i}))$. 
When $n_{i} = n$ (and $r_{i} = O(f(n))$),
all subsequent batches are similar to edge batching, i.e.,
$r_{i+1} = \eta_{e} r_{i}$ (and $n_{i+1} = n$).

This can be visualized on the space of subgraphs as sampling along the curve $f(|V|)$ from $|V| = n_0$ until $f(|V|)$ intersects $|V| = n$ and then sampling along the vertical line~$|V| = n$.
See~\figref{fig:ve_batching} and~\figref{fig:ve_comparitive} for a mental picture.
As we will see in our experiments, hybrid batching typically performs comparably (in terms of path quality) to vertex batching on easy problems and to edge batching on hard problems.

The intuition behind this is straightforward. If the problem is easy, then hybrid batching
finds a feasible solution early on, typically when the number of vertices
is similar to that needed by vertex batching for a feasible solution. Thus, the work
would be far less than that for edge batching.
On the other hand, if the problem is hard, then hybrid batching 
would have to get much closer to the~$|V| = n$ line before the dispersion becomes low enough to find a solution. However, it would
not involve as much work as for vertex batching, because the radius decreases when the number of vertices increases, unlike vertex batching which uses $r_i = \sqrt{d}$ for every iteration~$i$.


\section{Analysis for Halton Sequences}
\label{sec:densification-analysis}

In this section we consider the space of subgraphs and the densification strategies that we introduced in \sref{sec:densification-strategies} for the specific case that $\calS$ is a Halton sequence (\sref{sec:background-dispersion}).
We start by describing the boundaries of the starvation regions.
We then continue by simulating the bound on the quality of the solution obtained as a function of the work done for each of our strategies.

Recall that the since we are considering the unit hypercube $[0,1]^{d}$, then $\delta_{\text{max}} < \sqrt{d}$.
We use \eref{eq:dispersion_suboptimality} 
to first obtain bounds on the vertex  and edge starvation regions, and subsequently
analyze the tradeoff between work and solution quality for vertex and edge batching.

\subsection{Starvation region bounds}
\label{sec:densification-analysis-starvation}

To bound the vertex starvation region we wish to find~$n_{\text{min}}$ after which bounded sub-optimality can be guaranteed
to find the first solution. 
Note that $\delta^*$ is the clearance of the shortest path $\gamma*$ in $\calG$ connecting $s_1$ and $s_2$,
that~$p_d$ denotes the $d^{th}$ prime and $D_n \leq p_d / n^{1/d}$ for Halton sequences.
%
For~\eref{eq:dispersion_suboptimality} to hold we require that $2D_{n_{\text{min}}} < \delta^{*}$.
Thus,

\begin{equation}
\label{eq:vertex_starvation}
2D_{n_{\text{min}}} < \delta^{*} 
\Rightarrow
2\frac{p_d}{n_{\text{min}}^{1/d}} < \delta^{*}
\Rightarrow
n_{\text{min}} > \left( \frac{2p_d}{\delta^*}\right)^d
\end{equation}
Indeed, one can see that as the problem becomes harder (namely, $\delta^*$ decreases),
$n_{\text{min}}$ and 
the entire vertex-starvation region grows. 

We now  show that for Halton sequences, the edge-starvation region has a linear boundary, i.e. $f(|V|) = O(|V|)$.
Using \eref{eq:dispersion_suboptimality} we have that the minimal radius $r_{\min}(|V|)$ required for a graph with $|V|$ vertices is 
\begin{equation}
\label{eq:edge_starvation}
r_{\min}(|V|) > 2D_{|V|} 
\Rightarrow 
r_{\min}(|V|) > \frac{2 p_d}{(|V|)^{1/d}}.
\end{equation}
For any $r$-disk graph~$\calG(\ell, r)$, the number of edges is $|E_{\ell, r}| = O\left(\ell^2 \cdot r^{d}\right)$.
In our case,
\begin{equation}
\label{eq:f_v_line}
f(|V|) 
= O\left(|V|^2 \cdot r_{\min}^{d}(|V|)\right)
= O(|V|).
\end{equation}

\begin{marginfigure}
		
    \subfloat[Easy problem]{
        \includegraphics[width=\textwidth]{figures/densification/easy_comparison}
        \label{fig:ratio_easy}
    } \\
    \subfloat[Hard problem]{
        \includegraphics[width=\textwidth]{figures/densification/hard_comparison}
        \label{fig:ratio_hard}
    }

    \caption[Simulation of the work-suboptimality tradeoff]{\ A simulation of the work-suboptimality tradeoff for vertex, edge  and hybrid batching. 
  	Here we chose $n = 10^6$ and $d=4$. 
  	The easy and hard problems have
  	$\delta^{*} = \sqrt{d} /2$ and $\delta^{*} = 5D_{n}$, respectively.	
  	The plot is produced by sampling points along the curves $|V| = n$ and $|E| = |V| \cdot (|V|-1|)/2$ and using the respective values in \eref{eq:dispersion_suboptimality}.
  	Note that $x$-axis is in log-scale.
    }
	 \label{fig:ratio}
\end{marginfigure}

\subsection{Effort-to-Quality ratio}
\label{sec:densification-analysis-ratio}

We now compare our densification strategies in terms of their worst-case anytime performance. Specifically, we plot the cumulative amount of work as subgraphs are searched, measured by the maximum number of edges that may be evaluated, as a function of the bound on the quality of the solution that may be obtained using \eref{eq:dispersion_suboptimality}.
We fix a specific setting (namely $d$ and $n$) and simulate the work done and the suboptimality using the necessary formulae.
This is done for an easy and a hard problem. See~\figref{fig:ratio}. 

Indeed, this simulation coincides with our discussion on properties of both batching strategies with respect to the problem difficulty.
Vertex batching outperforms edge batching on easy problems and vice versa. Hybrid batching lies somewhere in between the two approaches
with the specifics depending on problem difficulty.


\section{Implementation}
\label{sec:densification-implementation}

Our analysis and exposition so far has been independent of any parameters for the densification or any other implementation decisions. In this section we outline the specifics of how we implement the densification strategies for anytime planning on roadmaps, before going into the results for the same.

\subsection{Densification Parameters}
\label{sec:densification-implementation-parameters}

We choose the parameters for each densification strategy such that the number of batches is $O(\text{log}_2n)$.

\subsubsection{Edge Batching}
\label{sec:densification-implementation-parameters-eb}

We set $\eta_e = 2^{1/d}$ . 
Recall that for $r$-disk graphs, the average degree of vertices is~$n \cdot r_{i}^{d}$, therefore this value (and hence the number of edges) is doubled after each iteration. 
We set $r_0 = 3\cdot n^{-1/d}$.

\subsubsection{Vertex Batching}
\label{sec:densification-implementation-parameters-vb}

We set the initial number of vertices~$n_0$ to be $100$,
irrespective of the roadmap size and problem setting,
and set $\eta_v = 2$.
After each batch
we double the number of vertices. 

\subsubsection{Hybrid Batching}
\label{sec:densification-implementation-parameters-hb}

The parameters are derived from those used for vertex and edge batching.
We begin with $n_0 = 100$, and after each batch we increase the vertices
by a factor of~$\eta_v = 2$. For these searches, i.e. in the region where $n_i < n$,
we use $r_i = 3 \cdot n^{-1/d}$. This ensures the same radius
at $n$ as for edge batching. Subsequently, we increase the radius as
$r_i = \eta_e r_{i-1}$, where $\eta_e = 2^{1/d}$.

\subsection{Optimizations}
\label{sec:densification-implementation-optimization}

For the densification strategies to be useful in practice, we employ certain optimizations.
To motivate this, consider the total work done (measured in edge evaluations) by the batching strategies in the worst case. 
Recall that for naively searching the entire roadmap, this work is ${n\choose2} \approx \frac{n^2}{2}$.
Furthermore for our worst-case analysis, we assume that both batching strategies run for $\text{log}_2 n$ iterations. 

For vertex batching, the number of
vertices at a given iteration is~$n_i = 2^i$ and the number of vertices is~$|E_i| \approx n_i^2 / 2 = 2^{2i-1}$.
The worst-case work complexity for vertex batching is:
\begin{equation}
\label{eq:vb_worstcase_work}
\sum\limits_{i=1}^{\text{log}_2n} |E_i| 
= 
\sum\limits_{i=1}^{\text{log}_2n} 
2^{2i-1}
=
\frac{2n^2}{3} + \text{lower order terms}
\end{equation}

For edge batching, with low-dispersion sequences, we have that $|E_{i}| \approx n2^{i}$. The worst-case work
complexity for edge batching is:
\begin{equation}
\label{eq:eb_worstcase_work}
\sum\limits_{i=1}^{\text{log}_2n} |E_i| 
=
\sum\limits_{i=1}^{\text{log}_2n} n2^{i}
=
2n^2 + \text{lower order terms}
\end{equation}
Note that hybrid batching's worst-case work would be  greater than edge batching, so we omit
the expression for brevity. 
Therefore, in all cases, the worst case work done by any batching strategy
is strictly larger than searching $\calG$ directly.
Thus, we consider numerous optimizations and their effect 
on the overall performance.


\subsubsection{Search Technique}
\label{sec:densification-implementation-optimization-search}

Each subgraph is searched using Lazy $\text{A}^{*}$~\citep{CPL14} with 
incremental rewiring as in $\text{LPA}^{*}$~\citep{koenig2004lifelong}. For details,
see the search algorithm used for a single batch of $\text{BIT}^{*}$~\citep{gammell2014batch}.
This lazy variant of $\text{A}^{*}$ has been shown to outperform other path-planning techniques for motion-planning search problems
with expensive edge evaluations~\citep{dellin2016unifying}.

\noindent
\textbf{N.B - } We use the search technique described above in~\citep{choudhury2016densification} to compare
directly with $\text{BIT}^{*}$. However, as mentioned in \cref{chapter:approach}, in this thesis we propose using POMP (\cref{chapter:cspacebelief}, \citet{choudhury2016pareto}) as the technique for searching each individual subgraph.


\subsubsection{Caching Collision Checks}
\label{sec:densification-implementation-optimization-caching}

Each time the collision-detector~$\calM$ is called for an edge, we store the ID of the edge along with the result using a hashing data structure. 
Subsequent calls for that specific edge are simply lookups in the hashing data structure which incur negligible running time.
Thus,~$\calM$ is called for each edge at most once.


\subsubsection{Sample Pruning and Rejection}
\label{sec:densification-implementation-optimization-ellipse}

For anytime algorithms, once an initial solution is obtained, subsequent searches should be focused
on the subset of states that could potentially improve the solution. When the space $\mathcal{X}$ is
Euclidean, this, so-called ``informed subset'', can be described 
by a prolate hyperspheroid~\citep{GSB14}. 
For our densification strategies, we prune away all existing vertices
(for all batching), and reject the newer vertices (for vertex and hybrid batching),
that fall outside the informed subset. 

Successive prunings due to intermediate solutions significantly reduces the average-case complexity
of future searches~\citep{gammell2014batch}, despite the extra time required to do so, which is accounted for in our benchmarking. Note that for Vertex and Hybrid Batching, which begin with only a few samples, samples in successive batches that are outside the current ellipse can just be rejected. This is cheaper than pruning, which is required  for Edge Batching. Across all test cases, we noticed poorer performance when pruning was omitted.

In the presence of obstacles, the extent to which the complexity is reduced due to pruning is difficult to obtain analytically. As shown in Theorem \ref{th:ellipse}, however, in the assumption of free space, we can derive results for Edge Batching. This motivates using this heuristic.

\begin{theorem}
\label{th:ellipse}

Running edge batching in an obstacle-free $d$-dimensional Euclidean space over
a roadmap constructed using a deterministic low-dispersion sequence with
$r_0 > 2D_{n}$ and $r_{i+1} = 2^{1/d} r_{i}$,
while 
using sample pruning and rejection 
makes the worst-case complexity
of the total search, measured in edge evaluations, $O(n^{1+ 1/d})$.
\end{theorem}

\input{chapters/proof_ellipse_shrinkage}
\noindent


\subsubsection{Path shortcutting with local clique search}
\label{sec:densification-implementation-optimization-shortcutting}

A useful local optimization technique that edge (and hybrid) batching allow for is searching the fully connected subgraph, i.e.
the clique induced by the vertices on an intermediate feasible path~\citep{geraerts2007creating}. The extent of possible improvement depends 
on the actual vertices and the obstacle distribution, but intuitively, the optimization is useful
for discovering longer edges early on that may be part of the globally optimal path. This can help address
our earlier comment that edge (and hybrid) batching may suffer if the optimal path has very long edges.

If Halton sequences are used, for sufficiently large $n$, as shown earlier, we can bound the sub-optimality
of intermediate path cost $c_\calG$ in terms of the cost of the global optimum with $\delta$-clearance, $c^{*}(\delta)$. 
Furthermore we can bound the number of vertices lying on an intermediate path to be $\kappa < \frac{c_\calG}{D(\calS)} < \frac{((1 + \epsilon(n,r))c^{*}(\delta)}{D(\calS)}$.
Searching for the shortest path on a clique of size $\kappa$ is $O(\kappa^2)$.



\section{Experiments}
\label{sec:densification-experiments}

Our implementations of the various strategies are based on the publicly available
OMPL~\citep{SMK12} implementation of $\text{BIT}^{*}$~\citep{gammell2014batch}. Other than the specific parameters
and optimizations mentioned earlier, we use the default parameters of $\text{BIT}^{*}$. 
Notably, we use the Euclidean distance heuristic, an approximately sorted
queue, and limit graph pruning to changes in path length greater than 1\%.


\begin{figure*}[t]
    \subfloat[$\mathbb{R}^{2}$ - Easy]{
        \includegraphics[width=0.24\textwidth]{figures/densification/hypercube/bigfont/hypercube_2d_10000_100obs33covg}
        \label{fig:results_2d_easy}
    }
    \subfloat[$\mathbb{R}^{2}$ - Hard]{
        \includegraphics[width=0.24\textwidth]{figures/densification/hypercube/bigfont/hypercube_2d_10000_1000obs75covg}
        \label{fig:results_2d_hard}
    }
    \subfloat[$\mathbb{R}^{4}$ - Easy]{
        \includegraphics[width=0.24\textwidth]{figures/densification/hypercube/bigfont/hypercube_4d_100000_500obs33covg}
        \label{fig:results_4d_easy}
    }
    \subfloat[$\mathbb{R}^{4}$ - Hard]{
        \includegraphics[width=0.24\textwidth]{figures/densification/hypercube/bigfont/hypercube_4d_100000_2500obs75covg}
        \label{fig:results_4d_hard}
    }
    \caption[Densification strategies in random unit hypercube problems]{Experimental results in random unit hybercube scenarios for
    \protect\tikz{\protect\node[fill=myblue,draw=black]{};}\; vertex batching
    \protect\tikz{\protect\node[fill=myred,draw=black]{};}\; edge batching, and \protect\tikz{\protect\node[fill=mygreen,draw=black]{};}\; hybrid batching.
    The $y$-axis is the ratio between the length of the path produced by the algorithm and length of $\gamma^*$ (the shortest path
    on $\calG$) for that problem. The naive strategy of searching the complete graph requires the following times to find a solution
    - \protect\subref{fig:results_2d_easy} $\mathbf{44}$s, \protect\subref{fig:results_2d_hard} $\mathbf{200}$s, \protect\subref{fig:results_4d_easy} $\mathbf{12}$s and \protect\subref{fig:results_4d_hard} $\mathbf{56}$s. In each case this is significantly more than the time for any other strategy to reach the optimum.
    Figure best viewed in color.
    }
    \label{fig:results_2d_4d}
\end{figure*}

\subsection{Random hypercube scenarios}
\label{sec:densification-experiments-hypercube}

The different batching strategies are compared to each other on problems in $\mathbb{R}^{d}$ for $d =2,4$.
The domain is the unit hypercube $[0,1]^{d}$ while the obstacles are randomly generated axis-aligned
$d$-dimensional hyper-rectangles. 
All problems have a start configuration of $[0.25,0.25,\ldots]$
and a goal configuration of $[0.75,0.75,\ldots]$.
We used the first $n = 10^{4}$ and $n = 10^{5}$ points of the Halton sequence for the~$\mathbb{R}^{2}$ 
and~$\mathbb{R}^{4}$ problems, respectively.

Two parameters of the obstacles are varied to approximate the notion of problem hardness
described earlier -- the number of obstacles and the fraction
of $\mathcal{X}$ which is in $\mathcal{X}_{obs}$, which we denote by $\zeta_{\text{obs}}$. 
An easy problem is one which
has fewer obstacles and a smaller value of $\zeta_{\text{forb}}$.
The converse is true for a hard problem. 
Specifically, in $\mathbb{R}^{2}$, we have 
easy problems with $100$ obstacles and $\zeta_{\text{obs}} = 0.33$, and 
hard problems with $1000$ obstacles and $\zeta_{\text{obs}} = 0.75$. 
In $\mathbb{R}^{4}$
we maintain the same values for $\zeta_{\text{obs}}$, but use $500$ and $3000$ obstacles for easy and hard problems, respectively. For each problem setting ($\mathbb{R}^{2}$/$\mathbb{R}^{4}$; easy/hard) we generate~$30$ different random
scenarios and evaluate each strategy with the same set of samples on each of them. 
Each random scenario has a different set of solutions, so we show a representative
result for each problem setting in \figref{fig:results_2d_4d}. 

The results align well with our intuition
about the relative performance of the densification strategies on easy and hard problems.
Note that the naive strategy of searching $\calG$ with $\text{A}^{*}$ directly requires considerably more time to report the 
optimum solution than any other strategy. We mention the numbers 
in the accompanying caption of \figref{fig:results_2d_4d} but avoid plotting them so
as not to stretch the figures. Note the reasonable performance of hybrid batching across problems and difficulty levels.


\begin{figure*}[t]
    \subfloat[]{
        \frame{\includegraphics[width=0.235\textwidth]{figures/densification/herb_prob1.png}}
        \label{fig:herbprob1}
    }
    \subfloat[]{
        \includegraphics[width=0.235\textwidth]{figures/densification/herb_plot1}
        \label{fig:herbplot1}
    }
    \subfloat[]{
        \frame{\includegraphics[width=0.235\textwidth]{figures/densification/herb_prob2.png}}
        \label{fig:herbprob2}
    }
    \subfloat[]{
        \includegraphics[width=0.235\textwidth]{figures/densification/herb_plot2}
        \label{fig:herbplot2}
    }
    \caption[Densification strategies for manipulation planning problems]{We show results on 2 manipulation problems for \protect\tikz{\protect\node[fill=myblue,draw=black]{};}\; vertex batching
    \protect\tikz{\protect\node[fill=myred,draw=black]{};}\; edge batching, \protect\tikz{\protect\node[fill=mygreen,draw=black]{};}\; hybrid batching
    and \protect\tikz{\protect\node[fill=mypurple,draw=black]{};}\; $\text{BIT}^{*}$. For each problem the goal configuration of the right
    arm is rendered translucent. Both of the problems are fairly constrained and non-trivial. The problem depicted in (\protect\subref{fig:herbprob2}) has a 
    large clear area in front of the starting configuration, which may allow for a long edge. This could explain the better performance
    of vertex batching. The naive strategy takes $25$s for (\protect\subref{fig:herbplot1}) and $44$s for (\protect\subref{fig:herbplot2}) respectively.
    }
    \label{fig:results_herb}
\end{figure*}

\subsection{Manipulation planning problems}
\label{sec:densification-experiments-manipulation}

We also run simulated experiments on HERB~\citep{srinivasa2010herb}, a mobile manipulator designed and built by the Personal Robotics Lab at Carnegie Mellon University. 
The planning problems are for the 7-DOF right arm, on the problem
scenarios shown in ~\figref{fig:results_herb}. 
We use a roadmap of $10^5$ vertices defined using a Halton sequence~$\calS$ which was generated using the first $7$ prime numbers.
In addition to the batching strategies, we also evaluate the performance
of $\text{BIT}^{*}$, using the same set of samples~$\calS$. $\text{BIT}^{*}$ had been shown to achieve anytime performance
superior to contemporary anytime algorithms. 
The hardness of the problems in terms of clearance
is difficult to visualize in terms of the C-space of the arm, but the goal regions
are considerably constrained. As our results show (~\figref{fig:results_herb}), all densification
strategies solve the difficult planning problem in reasonable time, and generally outperform the BIT* strategy
on the same set of samples.


\section{Discussion}
\label{sec:densification-discussion}

In this chapter we presented, analyzed and implemented several densification strategies for anytime geometric motion planning on large dense roadmaps. We provided theoretical motivation for these densification techniques, and showed that they outperform the naive search significantly on difficult planning problems.

In this work we demonstrate our analysis for the case where the set of samples is generated from a low-dispersion
deterministic sequence. 
A natural extension is to provide a similar analysis for a sequence of random i.i.d. samples.
Here, $f(|V|) = O\left(\log |V| \right)$~\citep{karaman2010incremental}
instead of $O(|V|)$.
When out of the starvation regions we would like to bound the quality obtained similar to the bounds provided by~\eref{eq:dispersion_suboptimality}.
A starting point would be to leverage recent results~\citep{DMB15} for Random Geometric Graphs
under expectation, albeit for a \emph{specific} radius $r$.

Another question we wish to pursue is alternative possibilities to traverse the subgraph space of $\calG$.
As depicted in~\figref{fig:ve_batching}, our densification strategies are essentially
ways to traverse this space . 
We discuss three techniques that traverse relevant boundaries of the space. But there are innumerable
trajectories that a strategy can follow to reach the optimum. It would be interesting
to compare our current batching methods, both theoretically and practically, to those that
go through the interior of the space.
